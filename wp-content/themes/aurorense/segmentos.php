<?php get_header(); ?>
		
		<strong class="dn">Navegação auxiliar</strong>
		<div class="cam cem cf">
			<img src="<?php bloginfo("template_url"); ?>/images/img-int.jpg" alt="Imagem ilustrativa com referente à serviços da Aurorense" class="imagem_titulo" />
			
			<!-- <div class="centro">
				<ul class="cam-list">
					<li class="item"><a href="#" title="Home" class="link">Home</a></li>
					<li class="item">O Grupo</li>
					<li class="item">Nossa História</li>
				</ul>
			</div> -->
		</div>
		
		<hr class="dn" />
		
		<strong class="dn">Conteúdo</strong>
		<section class="cont cem cf">
			<div class="centro cf">
			<?php 
			while( have_posts() ) : the_post();
				if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs();
			?>
			
				<header class="tits">
					<h2 class="tit"><?php the_title();?></h2>
					<!-- <small class="det">Muitos anos de trabalho e comprometimento</small> -->
				</header>
				
				<section class="box fl-lf">
					<?php the_content(); ?>
				</section>
			<?php endwhile; ?>
			
			<strong class="dn">Coluna com mais informações</strong>
			<aside class="col fl-rg">
				<figure>
					<?php $hab = get_field( 'campo_link' ); /*Variavel campo personalizado(link)*/
		             if ( $hab != '' ) : /*Se imagem estiver com link*/ 
                    ?> 
                     <a href="<?php the_field ( 'campo_link' ); ?>" title="<?php the_title(); ?>" class="link"><!--Puxar Link-->
		               <img src="<?php the_field( 'imagem_sidebar' ); ?>" alt="" class="img fl-lf" />					
		            </a>
                    <?php else :?>
                        <img src="<?php the_field( 'imagem_sidebar' ); ?>" alt="" class="img fl-lf" />	
                    <?php endif;?>
    
				</figure>
                
               <?php the_field('informações_sidebar');?>
			</aside>
			</div>
		</section>

<?php get_footer(); ?>