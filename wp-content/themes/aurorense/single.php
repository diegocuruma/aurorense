<?php get_header(); ?>
		
		<strong class="dn">Navegação auxiliar</strong>
		<div class="cam cem cf">
			<img src="<?php bloginfo("template_url"); ?>/images/img-int.jpg" alt="Imagem ilustrativa com referente à serviços da Aurorense" class="imagem_titulo" />
			
			<!-- <div class="centro">
				<ul class="cam-list">
					<li class="item"><a href="#" title="Home" class="link">Home</a></li>
					<li class="item">O Grupo</li>
					<li class="item">Nossa História</li>
				</ul>
			</div> -->
		</div>
		
		<hr class="dn" />
		
		<strong class="dn">Conteúdo</strong>
		<section class="cont cem cf">
			<div class="centro cf">
			<?php 
			while( have_posts() ) : the_post();
				if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs();
			?>
			
				<header class="tits">
					<h2 class="tit"><?php the_title();?></h2>
					<!-- <small class="det">Muitos anos de trabalho e comprometimento</small> -->
				</header>
				
				<section class="box fl-lf">
					<?php the_content(); ?>
				</section>
			<?php endwhile; ?>
			
			<strong class="dn">Coluna com mais informações</strong>
			<aside class="col fl-rg">
				<figure>
					<?php  the_post_thumbnail('not-int'); ?>
				</figure>
			</aside>
			</div>
		</section>

<?php get_footer(); ?>