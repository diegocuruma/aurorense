<?php include_once('header.php') ?>

		<strong class="dn">Painel destaque</strong>
		<div class="painel cem cf">
			<div class="slide">
				<ul>
					<?php 
						$ban = new WP_Query( array('post_type' => 'banner') ); 		
						while($ban->have_posts()) : $ban->the_post();
							$habilitar = get_field("habilitar_link");
					?>
                            
					<?php if($habilitar!=false) : ?>
                            
                            <li>                                
                                <a href="<?php the_field("campo_link"); ?>" title="<?php the_title_attribute(); ?>" rel="external" class="c-before">
                                    <?php the_post_thumbnail(); ?>
                                </a>
                            </li>
                              
                            <?php else : ?>
                              
                            <li>                                
                                <?php the_post_thumbnail(); ?>
                            </li>
                              
                           <?php endif; endwhile; wp_reset_postdata();?>
				</ul>
				<div id="nav"></div>
			</div>
		</div>
		
		<hr class="dn" />
		
		<strong class="dn">Conteúdo</strong>
		<section class="cont cem cf">
			<div class="centro cf">
				<article class="segmentos">
					<ul class="list">
						<li class="parafusos item">
							<a href="http://aurorense.com.br/?page_id=39" title="titulo" class="link">
<span class="img"></span>
<h2 class="tit">Parafusos e Afins</h2>
							</a>
						</li>
						<li class="construcao item">
							<a href="http://aurorense.com.br/?page_id=46" title="titulo" class="link">
<span class="img"></span>
<h2 class="tit">Construção Civil</h2>
							</a>
						</li>
						<li class="ferragens item">
							<a href="http://aurorense.com.br/?page_id=52" title="titulo" class="link">
<span class="img"></span>
<h2 class="tit">Ferragens e Ferramentas</h2>
							</a>
						</li>
						<li class="servicos item">
							<a href="http://aurorense.com.br/?page_id=59" title="titulo" class="link">
<span class="img"></span>
<h2 class="tit">Serviços Metalurgicos</h2>
							</a>
						</li>
					</ul>
				</article>
				
				<!-- <a href="http://aurorense.com.br/homolog/?page_id=46"> -->
					<figure class="banner fl-lf">
						<figcaption class="tit">PRODUTO EM DESTAQUE</figcaption>
						
						
						
						<?php 
							$des = new WP_Query( array('post_type' => 'destaque', 'showposts' => 1) ); 		
							while($des->have_posts()) : $des->the_post();
								$habilitar = get_field("habilitar_link");
						?>
                            
							
								
										<?php the_post_thumbnail('det-index'); ?>
								  
								
								  
							<?php endwhile; wp_reset_postdata();?>
						
						
						
						
						
						
					</figure>
				<!-- </a> -->
				
				<section class="not fl-lf">
					<header class="tits">
						<h2 class="tit">Notícias</h2>
						<small class="det">Últimas notícias de nossa empresa.</small>
					</header>
					
					<ul class="list">
						<?php
						$noticias = new wp_query(array('post_type'=>'post','showposts'=>2));
						if ( $noticias->have_posts() ) : while ( $noticias->have_posts() ) : $noticias->the_post(); 
						?>
						<li class="item">
							<a href="<?php the_permalink() ?>" title="<?php echo the_title(); ?>">
<?php  the_post_thumbnail('not-index'); ?>
<strong class="tit"><?php the_title(); ?></strong> <br />
<small class="data"><?php the_time('d/M/Y') ?></small> <br />
<?php except_limit(115); ?>
							</a>
						</li>
						
							<?php endwhile; ?>
							 
						<?php else: ?>
						<li class="item">
							<strong class="tit">Nada Encontrado</strong>
							<small class="data">Erro 404</small>
							<p class="txt">Lamentamos mas não foram encontrados artigos.</p>
						</li>
						<?php endif; ?>
					</ul>
				</section>
			</div>
		</section>

<?php include_once('footer.php'); ?>