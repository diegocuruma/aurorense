<form id="tao-contact" name="tao-contact" class="form fl_lf">
	<fieldset class="field">
		<label class="nome label">
			<input type="text" name="nome" placeholder="Nome" class="nome required input">
		</label>
		<label class="email label">
			<input type="email" name="email" placeholder="Email" class="email required input">
		</label>
		<label class="mensagem label">
			<textarea name="mensagem" placeholder="Mensagem" class="required"></textarea>
		</label>
		<input type="button" value="Enviar" class="enviar bt" />
	</fieldset>
</form>